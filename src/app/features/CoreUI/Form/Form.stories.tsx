import React, { FC, useCallback } from "react";
import { useForm, Controller } from "react-hook-form";
import { action } from "@storybook/addon-actions";

import "../../App/Root/Root.scss";
import FormInput from "../FormInput/FormInput";
import TwoColumnGrid from "../TwoColumnGrid/TwoColumnGrid";
import Label from "../Label/Label";
import Button from "../Button/Button";
import Toggle from "../Toggle/Toggle";
import Checkbox from "../Checkbox/Checkbox";
import Select from "../Select/Select";
import MultiSelect from "../MultiSelect/MultiSelect";

export default { title: "Form" };

interface FormData {
  firstname: string;
  lastname: string;
  email: string;
  phone?: string;
  marketing: boolean;
  marketing2: boolean;
  weeklytips: boolean;
  newfeatures: boolean;
}

interface Props {
  onSubmit: (data: FormData) => void;
}

const Form: FC<Props> = ({ onSubmit }) => {
  const { register, control, handleSubmit, errors } = useForm();
  const mapSubmitHandler = useCallback((data) => onSubmit(data), [onSubmit]);

  return (
    <form onSubmit={handleSubmit(mapSubmitHandler)}>
      <TwoColumnGrid>
        <FormInput
          fillWidth
          label="Firstname *"
          name="firstname"
          ref={register({ required: true })}
          error={errors.firstname && "First name is required."}
        />
        <FormInput
          fillWidth
          label="Lastname *"
          name="lastname"
          ref={register({ required: true })}
          error={errors.lastname && "Last name is required."}
        />
        <Label title="Marketing 2">
          <Toggle
            name="weeklytips"
            label="Send me weekly tips to help me improve the engagement on my store"
            ref={register({})}
          />
        </Label>
        <FormInput
          fillWidth
          label="Phone Number"
          name="phone"
          ref={register({})}
        />
        <FormInput
          fillWidth
          label="E-Mail *"
          name="email"
          type="email"
          ref={register({ required: true })}
          error={errors.email && "E-Mail is required."}
        />
        <Toggle
          name="newfeatures"
          label="I want to be the first to hear about new features"
          defaultValue
          ref={register({})}
        />
        <Checkbox
          name="customerAcceptsMarketing"
          label="Customer Accepts Marketing"
          ref={register({})}
        />
        <Controller
          name="role"
          as={
            <Select
              placeholder="Role"
              options={[
                { label: "Manager", value: "manager" },
                { label: "Developer", value: "developer" },
                { label: "Designer", value: "designer" },
              ]}
            />
          }
          control={control}
        />

        <Controller
          name="favoriteColors"
          as={
            <MultiSelect
              placeholder="Favorite colors"
              options={[
                { label: "Red", value: "red" },
                { label: "Blue", value: "blue" },
                { label: "Yellow", value: "yellow" },
              ]}
            />
          }
          control={control}
        />

        <Button size="big" type="submit">
          Submit
        </Button>
      </TwoColumnGrid>
    </form>
  );
};

export const normal = () => <Form onSubmit={action("submit")} />;
