import React, { useCallback, useState } from "react";
import DropDownMenu from "./DropDownMenu";
import Button from "../Button/Button";
import DropDownMenuItem from "./DropDownMenuItem";
import { action } from "@storybook/addon-actions";

export default {
  component: DropDownMenu,
  title: "DropDownMenu",
};

const Normal = () => {
  const [isOpen, setIsOpen] = useState(false);
  const handleToggleMenu = useCallback(() => setIsOpen(!isOpen), [isOpen]);

  const handleDuplicate = useCallback(() => {
    action("Duplicate")();
    setIsOpen(false);
  }, []);

  const handleDelete = useCallback(() => {
    action("Delete")();
    setIsOpen(false);
  }, []);

  return (
    <>
      <div style={{ width: 200, margin: 16, position: "relative" }}>
        <Button fillWidth onClick={handleToggleMenu}>
          {!isOpen ? "Open" : "Close"}
        </Button>

        <div
          style={{
            width: "100%",
            position: "absolute",
            top: "calc(100% + 8px)",
          }}
        >
          <DropDownMenu isOpen={isOpen} onClose={handleToggleMenu}>
            <DropDownMenuItem onSelect={handleDuplicate}>
              Duplicate
            </DropDownMenuItem>
            <DropDownMenuItem type="danger" onSelect={handleDelete}>
              Delete
            </DropDownMenuItem>
          </DropDownMenu>
        </div>
      </div>
    </>
  );
};

export const normal = () => {
  return <Normal />;
};
