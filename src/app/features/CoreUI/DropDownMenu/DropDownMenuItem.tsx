import React, { FC } from "react";

import styles from "./DropDownMenu.module.scss";
import useKeyHandler from "../../../../utils/hooks/useKeyHandler";
import classNames from "classnames";

interface Props {
  type?: "normal" | "danger";
  onSelect?: () => void;
}

const DropDownMenuItem: FC<Props> = ({
  type = "normal",
  onSelect,
  children,
}) => {
  const keyDownHandler = useKeyHandler({
    13: (e) => {
      e.preventDefault();
      onSelect && onSelect();
    }, // Enter
    32: onSelect, // Space
  });

  const classes = classNames(styles.menuItem, styles[type]);

  return (
    <li
      className={classes}
      tabIndex={-1}
      role="menuitem"
      onClick={onSelect}
      onKeyDown={keyDownHandler}
    >
      {children}
    </li>
  );
};

export default DropDownMenuItem;
