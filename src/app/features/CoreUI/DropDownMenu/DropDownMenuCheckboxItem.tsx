import React, { FC } from "react";

import styles from "./DropDownMenu.module.scss";
import useKeyHandler from "../../../../utils/hooks/useKeyHandler";
import CheckboxIcon from "../Icons/CheckboxIcon";
import Label from "../Label/Label";

interface Props {
  children: string;
  isActive?: boolean;
  onSelect?: () => void;
}

const DropDownMenuCheckboxItem: FC<Props> = ({
  isActive,
  onSelect,
  children,
}) => {
  const keyDownHandler = useKeyHandler({
    13: (e) => {
      e.preventDefault();
      onSelect && onSelect();
    }, // Enter
    32: onSelect, // Space
  });

  return (
    <li
      className={styles.checkboxItem}
      tabIndex={-1}
      role="menuitemcheckbox"
      onClick={onSelect}
      onKeyDown={keyDownHandler}
    >
      <Label title={children || ""} position="right">
        <CheckboxIcon isActive={isActive} />
      </Label>
    </li>
  );
};

export default DropDownMenuCheckboxItem;
