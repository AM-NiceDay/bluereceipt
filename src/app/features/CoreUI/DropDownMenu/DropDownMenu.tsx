import React, {
  FC,
  MouseEventHandler,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import classNames from "classnames";

import styles from "./DropDownMenu.module.scss";
import useKeyHandler from "../../../../utils/hooks/useKeyHandler";

interface Props {
  className?: string;
  isOpen: boolean;
  onClose: () => void;
}

const Button: FC<Props> = ({ className, children, isOpen, onClose }) => {
  const [activeItemIndex, setActiveItemIndex] = useState<number>(-1);

  const menu = useRef<HTMLUListElement>(null);
  const menuItems = useRef<HTMLLIElement[]>([]);
  const numberOfMenuItems = menuItems.current.length;

  useEffect(() => {
    if (menu.current) {
      menuItems.current = Array.from(
        menu.current.querySelectorAll('[role^="menuitem"]:not([disabled])')
      );
    }
  }, []);

  const focusMenuItemHandler = useCallback((index: number) => {
    const menuItem = menuItems.current[index];

    if (!menuItem) return;

    setActiveItemIndex(index);
    menuItem.focus();
  }, []);

  const handleFocusNext = useCallback(() => {
    const nextItemIndex =
      activeItemIndex < numberOfMenuItems - 1 ? activeItemIndex + 1 : 0;
    focusMenuItemHandler(nextItemIndex);
  }, [activeItemIndex, numberOfMenuItems, focusMenuItemHandler]);

  const handleFocusPrevious = useCallback(() => {
    const previousItemIndex =
      activeItemIndex > 0 ? activeItemIndex - 1 : numberOfMenuItems - 1;
    focusMenuItemHandler(previousItemIndex);
  }, [activeItemIndex, numberOfMenuItems, focusMenuItemHandler]);

  useEffect(() => {
    if (isOpen) {
      focusMenuItemHandler(0);
    }
  }, [isOpen, focusMenuItemHandler]);

  const handleKeyDown = useKeyHandler({
    40: handleFocusNext, // Arrow down
    38: handleFocusPrevious, // Arrow Up
    9: onClose, // Tab
    27: onClose, // Escape
  });

  const handleMouseOver = useCallback<MouseEventHandler<HTMLUListElement>>(
    (e) => {
      const menuItemIndex = menuItems.current.indexOf(
        e.target as HTMLLIElement
      );
      focusMenuItemHandler(menuItemIndex);
    },
    []
  );

  const classes = classNames(className, styles.menu, {
    [styles.close]: !isOpen,
  });

  return (
    <ul
      className={classes}
      ref={menu}
      role="menu"
      onKeyDown={handleKeyDown}
      onMouseOver={handleMouseOver}
    >
      {children}
    </ul>
  );
};

export default Button;
