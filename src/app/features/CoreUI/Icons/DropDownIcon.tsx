/* eslint-disable max-len */
import React, { FC } from "react";

const DropDownIcon: FC = () => (
  <svg
    width="24"
    height="24"
    viewBox="-8 -10 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path d="M4 4L0 0H8L4 4Z" fill="#BFCADA" />
  </svg>
);

export default DropDownIcon;
