import React, { useState } from "react";
import Select from "./Select";

export default {
  component: Select,
  title: "Select",
};

const options = [
  { label: "Manager", value: "manager" },
  { label: "Developer", value: "developer" },
  { label: "Designer", value: "designer" },
];

const Normal = () => {
  const [value, setValue] = useState();

  return (
    <div style={{ margin: 16 }}>
      <Select options={options} value={value} onChange={setValue} />
    </div>
  );
};

export const normal = () => {
  return <Normal />;
};

const WithPlaceholder = () => {
  const [value, setValue] = useState();

  return (
    <div style={{ margin: 16 }}>
      <Select
        placeholder="Role"
        options={options}
        value={value}
        onChange={setValue}
      />
    </div>
  );
};

export const withPlaceholder = () => {
  return <WithPlaceholder />;
};
