import React, { FC, useCallback, useRef, useState } from "react";
import DropDownMenu from "../DropDownMenu/DropDownMenu";
import DropDownMenuItem from "../DropDownMenu/DropDownMenuItem";

import styles from "./Select.module.scss";
import useOnClickOutside from "../../../../utils/hooks/useOnClickOutside";
import DropDownIcon from "../Icons/DropDownIcon";

interface Props {
  placeholder?: string;
  options: { label: string; value: string }[];
  value?: string;
  onChange?: (newValue: string) => void;
}

const Select: FC<Props> = ({ placeholder, options, value, onChange }) => {
  const selectedOption = options.find((option) => option.value === value);

  const [isOpen, setIsOpen] = useState(false);
  const handleToggleMenu = useCallback(() => setIsOpen(!isOpen), [isOpen]);

  const button = useRef<HTMLButtonElement>(null);
  const closeHandler = useCallback(() => {
    setIsOpen(false);
    button.current?.focus();
  }, []);

  const createChangeHandler = useCallback(
    (value: string) => () => {
      closeHandler();
      onChange && onChange(value);
    },
    [onChange]
  );

  const select = useRef(null);
  useOnClickOutside(select, () => setIsOpen(false));

  return (
    <div className={styles.container} ref={select}>
      <button
        type="button"
        className={styles.selectButton}
        ref={button}
        onClick={handleToggleMenu}
      >
        {(selectedOption && selectedOption.label) ||
          placeholder ||
          "Select Value"}
        <DropDownIcon />
      </button>

      <DropDownMenu
        className={styles.menu}
        isOpen={isOpen}
        onClose={closeHandler}
      >
        {options.map((option) => (
          <DropDownMenuItem onSelect={createChangeHandler(option.value)}>
            {option.label}
          </DropDownMenuItem>
        ))}
      </DropDownMenu>
    </div>
  );
};

export default Select;
