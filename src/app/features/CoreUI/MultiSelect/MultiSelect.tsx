import React, { FC, useCallback, useRef, useState } from "react";
import DropDownMenu from "../DropDownMenu/DropDownMenu";

import styles from "./MultiSelect.module.scss";
import useOnClickOutside from "../../../../utils/hooks/useOnClickOutside";
import DropDownIcon from "../Icons/DropDownIcon";
import DropDownMenuCheckboxItem from "../DropDownMenu/DropDownMenuCheckboxItem";

interface Props {
  placeholder?: string;
  options: { label: string; value: string }[];
  value?: string[];
  onChange?: (newValues: string[]) => void;
}

const MultiSelect: FC<Props> = ({ placeholder, options, value, onChange }) => {
  const selectedOptions = options.filter((option) =>
    value?.includes(option.value)
  );

  const [isOpen, setIsOpen] = useState(false);
  const handleToggleMenu = useCallback(() => setIsOpen(!isOpen), [isOpen]);

  const button = useRef<HTMLButtonElement>(null);
  const closeHandler = useCallback(() => {
    setIsOpen(false);
    button.current?.focus();
  }, []);

  const createChangeHandler = useCallback(
    (newValue: string) => () => {
      if (!onChange) return;

      if (value?.includes(newValue)) {
        return onChange(value?.filter((value) => value !== newValue));
      }

      onChange && onChange([...(value || []), newValue]);
    },
    [value, onChange]
  );

  const select = useRef(null);
  useOnClickOutside(select, () => setIsOpen(false));

  return (
    <div className={styles.container} ref={select}>
      <button
        type="button"
        className={styles.selectButton}
        ref={button}
        onClick={handleToggleMenu}
      >
        {(selectedOptions &&
          selectedOptions.map((option) => option.label).join(", ")) ||
          placeholder ||
          "Select Value"}
        <DropDownIcon />
      </button>

      <DropDownMenu
        className={styles.menu}
        isOpen={isOpen}
        onClose={closeHandler}
      >
        {options.map((option) => (
          <DropDownMenuCheckboxItem
            isActive={selectedOptions.includes(option)}
            onSelect={createChangeHandler(option.value)}
          >
            {option.label}
          </DropDownMenuCheckboxItem>
        ))}
      </DropDownMenu>
    </div>
  );
};

export default MultiSelect;
