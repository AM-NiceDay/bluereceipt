import React, { useState } from "react";
import MultiSelect from "./MultiSelect";

export default {
  component: MultiSelect,
  title: "MultiSelect",
};

const options = [
  { label: "Red", value: "red" },
  { label: "Blue", value: "blue" },
  { label: "Yellow", value: "yellow" },
];

const Normal = () => {
  const [value, setValues] = useState<string[]>([]);

  return (
    <div style={{ margin: 16 }}>
      <MultiSelect options={options} value={value} onChange={setValues} />
    </div>
  );
};

export const normal = () => {
  return <Normal />;
};

const WithPlaceholder = () => {
  const [values, setValues] = useState<string[]>([]);

  return (
    <div style={{ margin: 16 }}>
      <MultiSelect
        placeholder="Role"
        options={options}
        value={values}
        onChange={setValues}
      />
    </div>
  );
};

export const withPlaceholder = () => {
  return <WithPlaceholder />;
};
