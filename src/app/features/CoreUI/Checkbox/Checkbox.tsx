import React, {
  FC,
  forwardRef,
  Ref,
  useCallback,
  useRef,
  useState,
} from "react";

import Label from "../Label/Label";
import CheckboxIcon from "../Icons/CheckboxIcon";
import useHover from "../../../../utils/hooks/useHover";
import useFocus from "../../../../utils/hooks/useFocus";
import useCombinedRefs from "../../../../utils/hooks/useCombinedRefs";
import styles from "./Checkbox.module.scss";

interface Props {
  /**
   * Identifier for form submit
   */
  name?: string;

  /**
   * Label to be displayed alongside with toggle input
   */
  label?: string;

  /**
   * Default value of toggle input, does not make the input controlled
   */
  defaultValue?: boolean;

  /**
   * Read only mode. Default: false
   */
  disabled?: boolean;

  value?: boolean;

  /**
   * Register callback for change event
   */
  onChange?: (newChecked: boolean) => void;

  /**
   * React ref passtrough to input node
   */
  ref?: Ref<HTMLInputElement>;
}

const Checkbox: FC<Props> = forwardRef((props, ref) => {
  const {
    label,
    defaultValue,
    disabled,
    value,
    onChange,
    ...otherProps
  } = props;

  const [isChecked, setChecked] = useState(!!defaultValue);

  const toggle = useCallback(() => {
    const newValue = !isChecked;
    setChecked(newValue);

    if (onChange) {
      onChange(newValue);
    }
  }, [isChecked, onChange]);

  const checkboxRef = useRef<HTMLLabelElement>(null);
  const hasHover = useHover(checkboxRef);

  const innerRef = useRef<HTMLInputElement>(null);
  const inputRef = useCombinedRefs(ref, innerRef);
  const hasFocus = useFocus(inputRef);

  return (
    <Label
      title={label || ""}
      disabled={disabled}
      position="right"
      ref={checkboxRef}
    >
      <CheckboxIcon
        isActive={isChecked}
        hasHover={hasHover || hasFocus}
        disabled={disabled}
      />
      <input
        type="checkbox"
        className={styles.input}
        ref={inputRef}
        checked={value || isChecked}
        disabled={disabled}
        onChange={toggle}
        {...otherProps}
      />
    </Label>
  );
});

export default Checkbox;
