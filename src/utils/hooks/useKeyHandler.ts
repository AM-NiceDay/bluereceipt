import { KeyboardEventHandler } from "react";

function useKeyHandler<T extends Element>(handlerByKeys: {
  [key: number]: KeyboardEventHandler<T> | undefined;
}): KeyboardEventHandler<T> {
  return (e) => {
    const keyHandler = handlerByKeys[e.keyCode];
    keyHandler && keyHandler(e);
  };
}

export default useKeyHandler;
